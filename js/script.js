
$(document).ready(function() {

    $('header .burger').click(function () {
        $('.wrap__menu').toggleClass('active');
        $('body').toggleClass('open-menu');
        $('header').toggleClass('active');
    });


    $("header .menu__item").on("click","a", function (event) {
                event.preventDefault();
                var id  = $(this).attr('href'),
                    top = $(id).offset().top;
                $('body,html').animate({scrollTop: top - 100}, 1500);
                 $('.wrap__menu').removeClass('active');
                  $('body').removeClass('open-menu');
                    $('header').removeClass('active');
            });
        

        var scroll = $(window).scrollTop();
        if (scroll >= 10) {
            $("header").addClass("fixed");
        } else {
            $("header").removeClass("fixed");
        }
        $(window).scroll(function() {
            var scroll = $(window).scrollTop();
            if (scroll >= 10) {
                $("header").addClass("fixed");
            } else {
                $("header").removeClass("fixed");
            }
        });



        if($('.partners__carousel').length) {
            $('.partners__carousel').owlCarousel({
                loop: true,
              items:5,     
              autoplay: true,
              autoplayTimeout: 6000,
              dots: false,
              nav: true,
              smartSpeed: 1500,
              responsive : {
                  0 : {
                      items:1    
                  },
                  490 : {
                      items:2
                  },
                  768 : {
                      items:2
                  },
                  992 : {
                      items:3
                  },
                  1199 : {
                      items:4
                  },
                  1440 : {
                      items:5
                  }
              }
          });
        }
      
	
});